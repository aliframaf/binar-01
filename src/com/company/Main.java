package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int Menu;
        float Panjang, Lebar, Sisi, Alas, Tinggi, Phi, Jarijari, LuasP, LuasL, LuasS, LuasPP, VolumeK, VolumeB, VolumeT;
        while (true) {

            //tampilan menu
            System.out.println("-------------------------------------");
            System.out.println("Kalkulator Penghitung Luas dan Volume");
            System.out.println("-------------------------------------");
            System.out.println("Menu");
            System.out.println("1. Hitung Luas Bidang");
            System.out.println("2. Hitung Luas Volume");
            System.out.println("0. Tutup Aplikasi");
            System.out.println("-------------------------------------");
            System.out.println("Input Menu : ");
            Menu = input.nextInt();

            if (Menu == 1) {
            // tampilan luas bidang pada menu
                System.out.println("-------------------------------------");
                System.out.println("Pilih Bidang yang akan dihitung");
                System.out.println("-------------------------------------");
                System.out.println("1. Persegi ");
                System.out.println("2. Lingkaran");
                System.out.println("3. Segitiga");
                System.out.println("4. Persegi Panjang");
                System.out.println("0. Kembali Ke Menu Sebelumnya");
                System.out.println("-------------------------------------");
                System.out.println("Input Menu : ");
                Menu = input.nextInt();

                //pilihan masing-masing luas
                if (Menu == 1) {
                    System.out.println();
                    System.out.println("-------------------------------------");
                    System.out.println("Anda Memilih luas persegi");
                    System.out.println("-------------------------------------");
                    System.out.println("Masukan nilai sisi : ");
                    Sisi = input.nextFloat();
                    LuasP = Sisi * Sisi;
                    System.out.println("Luas Persegi adalah : " + LuasP);
                    System.out.println();
                } else if (Menu == 2) {
                    System.out.println();
                    System.out.println("-------------------------------------");
                    System.out.println("Anda Memilih luas lingkaran");
                    System.out.println("-------------------------------------");
                    System.out.println("Masukan nilai phi : ");
                    Phi = input.nextFloat();
                    System.out.println("Masukan nilai jari-jari : ");
                    Jarijari = input.nextFloat();
                    LuasL = Phi * Jarijari;
                    System.out.println("Luas Lingkaran adalah : " + LuasL);
                    System.out.println();
                } else if (Menu == 3) {
                    System.out.println();
                    System.out.println("-------------------------------------");
                    System.out.println("Anda Memilih luas Segitiga");
                    System.out.println("-------------------------------------");
                    System.out.println("Masukan nilai Alas : ");
                    Alas = input.nextInt();
                    System.out.println("Masukan nilai tinggi : ");
                    Tinggi = input.nextInt();
                    LuasS = Alas * Tinggi / 2;
                    System.out.println("Luas Segitiga adalah :" + LuasS);
                    System.out.println();
                } else if (Menu == 4) {
                    System.out.println();
                    System.out.println("-------------------------------------");
                    System.out.println("Anda Memilih luas Persegi Panjang");
                    System.out.println("-------------------------------------");
                    System.out.println("Masukan nilai Panjang : ");
                    Panjang = input.nextInt();
                    System.out.println("Masukan nilai Lebar : ");
                    Lebar = input.nextInt();
                    LuasPP = Panjang * Lebar;
                    System.out.println("Luas Persegi Panjang adalah :" + LuasPP);
                    System.out.println();
                } else if (Menu == 0) {

                }
            }
            //pilihan bidang volume pada menu
            else if (Menu == 2){
                System.out.println("-------------------------------------");
                System.out.println("Pilih Bidang yang akan dihitung");
                System.out.println("-------------------------------------");
                System.out.println("1. Kubus ");
                System.out.println("2. Balok ");
                System.out.println("3. Tabung ");
                System.out.println("0. Kembali Ke Menu Sebelumnya");
                System.out.println("-------------------------------------");
                System.out.println("Input Menu : ");
                Menu = input.nextInt();

                //perhitungan masing-masing bidang
                if (Menu == 1) {
                    System.out.println();
                    System.out.println("-------------------------------------");
                    System.out.println("Anda Memilih Volume Kubus");
                    System.out.println("-------------------------------------");
                    System.out.println("Masukan nilai sisi 1: ");
                    Sisi= input.nextFloat();
                    System.out.println("Maukan nilai sisi 2 : ");
                    Sisi= input.nextFloat();
                    System.out.println("Maukan nilai sisi 3 : ");
                    Sisi = input.nextFloat();
                    VolumeK = Sisi * Sisi * Sisi;
                    System.out.println("Volume Kubus adalah : " + VolumeK);
                    System.out.println();

                } else if (Menu == 2) {
                    System.out.println();
                    System.out.println("-------------------------------------");
                    System.out.println("Anda Memilih Volume balok");
                    System.out.println("-------------------------------------");
                    System.out.println("Masukan nilai panjang: ");
                    Panjang = input.nextFloat();
                    System.out.println("Maukan nilai lebar : ");
                    Lebar = input.nextFloat();
                    System.out.println("Maukan nilai tinggi : ");
                    Tinggi = input.nextFloat();
                    VolumeB = Panjang * Lebar * Tinggi;
                    System.out.println("Volume Balok adalah : " + VolumeB);
                    System.out.println();

                } else if (Menu == 3) {
                    System.out.println();
                    System.out.println("-------------------------------------");
                    System.out.println("Anda Memilih Volume Tabung");
                    System.out.println("-------------------------------------");
                    System.out.println("Masukan nilai phi: ");
                    Phi = input.nextFloat();
                    System.out.println("Maukan nilai Jari-Jari : ");
                    Jarijari = input.nextFloat();
                    System.out.println("Maukan nilai tinggi : ");
                    Tinggi = input.nextFloat();
                    VolumeT = Phi * Jarijari * Tinggi;
                    System.out.println("Volume Tabung adalah : " + VolumeT);
                    System.out.println();
                }

            } else if (Menu == 0) {
                System.exit(Menu);
            }}}}